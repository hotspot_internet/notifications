package hotspotinternet.notifications.email.impl;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.time.Instant;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Optional;

/**
 * Class for storing information of a data usage report. It describes an instant
 * at which the data usage is known.
 */
public class UsageReport {
    private static final Pattern PERCENT_REMAIN_PATTERN =
            Pattern.compile(".*?([0-9]+)% remain", Pattern.CASE_INSENSITIVE);
    private static final Pattern PHONE_NUMBER_ENDING_PATTERN =
            Pattern.compile(".*ending in ([0-9]+)", Pattern.CASE_INSENSITIVE);
    private static final Pattern PHONE_NUMBER_PATTERN =
            Pattern.compile(
                    ".*?number ((?:\\(?[0-9]{3}\\)?)?[ ]?[0-9]{3}[\\- ]?[0-9]{4})",
                    Pattern.CASE_INSENSITIVE);
    private static final Pattern REACHED_ALLOWED_PATTERN =
            Pattern.compile("(?:has reached)|(?:all out)", Pattern.CASE_INSENSITIVE);

    private Long usageReportId;
    private String messageId;
    private String source;
    private Instant instant;
    private Long usedBytes;
    private String phoneNumber;
    private Double usedPercent;

    /**
     * No-arg empty constructor to allow marshalling to/from JSON.
     */
    public UsageReport() { }

    /**
     * Constructs UsageReport from provided arguments. 1 and only one of
     * usedPercent and usedBytes must be non-null.
     *
     * @param usageReportId ID for storing ID in database
     * @param messageId ID of message received from source
     * @param source from which report received
     * @param instant at which report originated
     * @param usedBytes bytes used
     * @param phoneNumber of report
     * @param usedPercent associated with report
     */
    @VisibleForTesting
    public UsageReport(
            final Long usageReportId,
            final String messageId,
            final String source,
            final Instant instant,
            final Long usedBytes,
            final String phoneNumber,
            final Double usedPercent) {
        checkArgument(
                usedBytes == null ^ usedPercent == null,
                "1 and only 1 of usedPercent and usedBytes must be non-null");
        this.messageId = checkNotNull(messageId);
        this.source = checkNotNull(source);
        this.instant = checkNotNull(instant);
        this.phoneNumber = checkNotNull(phoneNumber);
        this.usageReportId = usageReportId;
        this.usedPercent = usedPercent;
        this.usedBytes = usedBytes;
    }

    /**
     * Creates UsageReport from provided arguments or returns absent optional
     * if message text does not correspond to usage report.
     *
     * @param messageId ID of message
     * @param source email address from which usage e-mail was received
     * @param instant at which usage e-mail was received
     * @param messageText plain-text contents of usage e-mail
     * @return UsageReport or absent optional if message text is not usage report
     */
    public static Optional<UsageReport> from(
            final String messageId,
            final String source,
            final Instant instant,
            final String messageText) {
        String percentRemain = getGroupMatch(PERCENT_REMAIN_PATTERN, messageText);
        String phoneNumber = getGroupMatch(PHONE_NUMBER_PATTERN, messageText);
        String phoneNumberEnding = getGroupMatch(PHONE_NUMBER_ENDING_PATTERN, messageText);
        boolean hasReachedLimit = REACHED_ALLOWED_PATTERN.matcher(messageText).find();

        if ((percentRemain != null || hasReachedLimit)
                && (phoneNumber != null || phoneNumberEnding != null)) {
            return Optional.of(new UsageReport(
                    null,
                    messageId,
                    source,
                    instant,
                    null,
                    phoneNumberEnding != null ? phoneNumberEnding : phoneNumber,
                    hasReachedLimit ? 100.0 : 100 - Double.valueOf(percentRemain)));
        }
        return Optional.absent();
    }

    private static String getGroupMatch(final Pattern pattern, final String messageText) {
        Matcher matcher = pattern.matcher(messageText);
        return matcher.find() && matcher.groupCount() > 0
                ? matcher.group(1)
                : null;
    }

    public String getSource() {
        return source;
    }

    public String getMessageId() {
        return messageId;
    }

    public Double getUsedPercent() {
        return usedPercent;
    }

    public Long getUsedBytes() {
        return usedBytes;
    }

    public Instant getInstant() {
        return instant;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Long getUsageReportId() {
        return usageReportId;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("Report ID", usageReportId)
                .add("Message ID", messageId)
                .add("Source", source)
                .add("Used Bytes", usedBytes)
                .add("Used Percent", usedPercent)
                .add("Phone Number", phoneNumber)
                .add("Instant", instant)
                .toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof UsageReport)) {
            return false;
        }
        UsageReport otherUsageReport = (UsageReport) other;
        return Objects.equal(usageReportId, otherUsageReport.usageReportId)
                && Objects.equal(messageId, otherUsageReport.messageId)
                && Objects.equal(source, otherUsageReport.source)
                && Objects.equal(usedBytes, otherUsageReport.usedBytes)
                && Objects.equal(usedPercent, otherUsageReport.usedPercent)
                && Objects.equal(phoneNumber, otherUsageReport.phoneNumber)
                && Objects.equal(instant, otherUsageReport.instant);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
                usageReportId,
                messageId,
                source,
                usedBytes,
                usedPercent,
                phoneNumber,
                instant);
    }
}

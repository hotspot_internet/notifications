package hotspotinternet.notifications.email.impl;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.diffplug.common.base.Throwing.Specific;
import com.diffplug.common.base.Throwing.Specific.Consumer;
import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.typesafe.config.Config;

import hotspotinternet.notifications.email.Email;
import hotspotinternet.notifications.email.EmailClient;

/**
 * Implementation for retrieving/sending emails via configured email account.
 */
public class EmailClientImpl implements EmailClient {
    static final String PROTOCOL = "imap";

    private static final Logger LOG = LoggerFactory.getLogger(EmailClientImpl.class);
    private static final String HOST = "imap.gmail.com";
    private static final String INBOX = "INBOX";
    private static final int PORT = 993;
    private static final Properties PROPERTIES = new Properties();
    private static final Properties SMTP_PROPERTIES = new Properties();
    private static final String NOTIFICATION_DESTINATIONS_KEY = "notifications.email.destinations";
    private static final String EMAIL_ADDRESS_KEY = "notifications.email.address";
    private static final String EMAIL_PASSWORD_KEY = "notifications.email.password";

    static {
        // server setting
        PROPERTIES.put(String.format("mail.%s.host", PROTOCOL), HOST);
        PROPERTIES.put(String.format("mail.%s.port", PROTOCOL), PORT);
        // SSL setting
        PROPERTIES.setProperty(
                String.format("mail.%s.socketFactory.class", PROTOCOL),
                "javax.net.ssl.SSLSocketFactory");
        PROPERTIES.setProperty(
                String.format("mail.%s.socketFactory.fallback", PROTOCOL),
                "false");
        PROPERTIES.setProperty(
                String.format("mail.%s.socketFactory.port", PROTOCOL),
                String.valueOf(PORT));

        SMTP_PROPERTIES.put("mail.smtp.host", "smtp.gmail.com");
        SMTP_PROPERTIES.put("mail.smtp.port", "587");
        SMTP_PROPERTIES.put("mail.smtp.auth", "true");
        SMTP_PROPERTIES.put("mail.smtp.starttls.enable", "true");
    }

    private final List<InternetAddress> internetAddresses;
    private final Consumer<Message, MessagingException> messageSender;
    private final Specific.Function<Properties, Store, NoSuchProviderException> storeFunction;
    private final String emailAddress;
    private final String emailPassword;
    private final Function<Message, Optional<UsageReport>> usageReportFunction;

    private Folder inboxFolder;
    private Store store;
    private boolean isOpen;

    @Inject
    EmailClientImpl(
            final Config config,
            final Specific.Function<Properties, Store, NoSuchProviderException> storeFunction,
            final Consumer<Message, MessagingException> messageSender,
            final Function<Message, Optional<UsageReport>> usageReportFunction) {
        this.internetAddresses = getInternetAddresses(
                config.getStringList(NOTIFICATION_DESTINATIONS_KEY));
        this.emailAddress = config.getString(EMAIL_ADDRESS_KEY);
        this.emailPassword = config.getString(EMAIL_PASSWORD_KEY);
        this.messageSender = messageSender;
        this.storeFunction = storeFunction;
        this.usageReportFunction = usageReportFunction;
    }

    private static List<InternetAddress> getInternetAddresses(
            final List<String> internetAddressStrings) {
        return internetAddressStrings
                .stream()
                .map(internetAddressString -> {
                    try {
                        return new InternetAddress(internetAddressString);
                    } catch (AddressException ex) {
                        throw new IllegalArgumentException(ex);
                    }
                })
                .collect(Collectors.toList());
    }

    @Override
    public void run() throws IOException {
        synchronized (storeFunction) {
            if (!isOpen) {
                try {
                    store = storeFunction.apply(PROPERTIES);
                    store.connect(emailAddress, emailPassword);

                    inboxFolder = store.getFolder(INBOX);
                    inboxFolder.open(Folder.READ_ONLY);
                    isOpen = true;
                } catch (MessagingException ex) {
                    throw new IOException(ex);
                }
            }
        }
    }

    @Override
    public void close() throws IOException {
        synchronized (storeFunction) {
            if (isOpen) {
                try {
                    inboxFolder.close(false);
                    store.close();
                } catch (MessagingException ex) {
                    throw new IOException(ex);
                } finally {
                    isOpen = false;
                }
            }
        }
    }

    @Override
    public List<UsageReport> getUsageReports() {
        final Instant oneMonthAgo = Instant.now().minus(Duration.ofDays(31));
        List<Message> messages;
        try {
            if (!inboxFolder.isOpen()) {
                inboxFolder.open(Folder.READ_ONLY);
            }
            messages = Arrays.asList(inboxFolder.getMessages());
        } catch (MessagingException ex) {
            LOG.warn("Error getting messages", ex);
            return Collections.emptyList();
        }
        return messages
                .stream()
                .map(usageReportFunction)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .filter(usageReport -> usageReport.getInstant().isAfter(oneMonthAgo))
                .collect(Collectors.toList());
    }

    static Optional<UsageReport> getUsageReport(final Message message) {
        final MimeMessage mimeMessage;
        if (message instanceof MimeMessage) {
            mimeMessage = (MimeMessage) message;
        } else {
            LOG.warn("Message is not of type 'MimeMessage' - cannot parse");
            return Optional.absent();
        }

        try {
            return UsageReport.from(
                    mimeMessage.getMessageID(),
                    mimeMessage.getFrom()[0].toString(),
                    mimeMessage.getSentDate().toInstant(),
                    getTextFromMessage(mimeMessage));
        } catch (IOException | MessagingException ex) {
            LOG.warn("Error parsing message", ex);
            return Optional.absent();
        }
    }

    private static String getTextFromMessage(Message message)
            throws MessagingException, IOException {
        if (message.isMimeType("text/plain")) {
            return message.getContent().toString();
        } else if (message.isMimeType("multipart/*")) {
            MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
            return getTextFromMimeMultipart(mimeMultipart);
        }
        throw new IOException(String.format(
                "Unhandled content type of '%s'",
                message.getContentType()));
    }

    private static String getTextFromMimeMultipart(
            MimeMultipart mimeMultipart)  throws MessagingException, IOException {
        final StringBuilder resultBuilder = new StringBuilder();

        int count = mimeMultipart.getCount();
        for (int partIndex = 0; partIndex < count; ++partIndex) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(partIndex);
            if (bodyPart.isMimeType("text/plain")) {
                resultBuilder
                        .append("\n")
                        .append(bodyPart.getContent());
                break; // without break same text appears twice
            } else if (bodyPart.isMimeType("text/html")) {
                String html = (String) bodyPart.getContent();
                resultBuilder
                        .append("\n")
                        .append(org.jsoup.Jsoup.parse(html).text());
            } else if (bodyPart.getContent() instanceof MimeMultipart) {
                resultBuilder.append(getTextFromMimeMultipart(
                        (MimeMultipart) bodyPart.getContent()));
            }
        }
        return resultBuilder.toString();
    }

    private Authenticator createAuthenticator() {
        return new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailAddress, emailPassword);
            }
        };
    }

    @Override
    public void send(Email email) throws MessagingException {
        Message message = new MimeMessage(Session.getInstance(
                SMTP_PROPERTIES,
                createAuthenticator()));
        message.setFrom(new InternetAddress(emailAddress));
        message.setRecipients(
                Message.RecipientType.TO,
                internetAddresses.toArray(new InternetAddress[internetAddresses.size()]));
        message.setSubject(email.getSubject());

        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(email.getContent(), "text/html");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);

        message.setContent(multipart);

        messageSender.accept(message);
    }
}

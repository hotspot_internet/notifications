package hotspotinternet.notifications.email.impl;

import java.util.Properties;
import java.util.function.Function;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;

import com.diffplug.common.base.Throwing.Specific;
import com.google.common.base.Optional;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;

import hotspotinternet.notifications.email.EmailClient;

/**
 * Guice module for binding implementations for interfaces.
 */
public class EmailClientModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(EmailClient.class).to(EmailClientImpl.class);

        // bindings for EmailClientImpl constructor
        bind(new TypeLiteral<Specific.Consumer<Message, MessagingException>>(){})
                .toInstance(Transport::send);
        bind(new TypeLiteral<Specific.Function<Properties, Store, NoSuchProviderException>>(){})
                .toInstance((properties) -> Session
                        .getDefaultInstance(properties)
                        .getStore(EmailClientImpl.PROTOCOL));
        bind(new TypeLiteral<Function<Message, Optional<UsageReport>>>(){})
                .toInstance(EmailClientImpl::getUsageReport);
    }
}

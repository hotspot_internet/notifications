package hotspotinternet.notifications.email;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import javax.mail.MessagingException;

import com.diffplug.common.base.Throwing.Specific.Runnable;

import hotspotinternet.notifications.email.impl.UsageReport;

/**
 * Interface for retrieving/sending emails via configured email account.
 */
public interface EmailClient extends Runnable<IOException>, Closeable {
    /**
     * Get data usage reports from configured email account.
     *
     * @return list of usage reports
     */
    List<UsageReport> getUsageReports();

    /**
     * Sends email to all configured recipients.
     *
     * @param email to be sent
     * @throws MessagingException if problem sending email
     */
    void send(Email email) throws MessagingException;
}

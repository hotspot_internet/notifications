
/**
 * Interfaces for collecting data usage notifications from email inbox.
 */
package hotspotinternet.notifications.email;

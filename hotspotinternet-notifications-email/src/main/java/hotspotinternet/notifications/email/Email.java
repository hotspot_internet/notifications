package hotspotinternet.notifications.email;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Interface for an email message.
 */
public interface Email {
    /**
     * Get content of email.
     *
     * @return content of email
     */
    String getContent();

    /**
     * Get subject of email.
     *
     * @return subject of email
     */
    String getSubject();

    /**
     * Creates email.
     *
     * @param content of email
     * @param subject of email
     * @return email
     */
    static Email createEmail(final String content, final String subject) {
        return new Email() {
            @Override
            public String getSubject() {
                return subject;
            }

            @Override
            public String getContent() {
                return content;
            }

            @Override
            public String toString() {
                return MoreObjects.toStringHelper(Email.class)
                        .add("Subject", getSubject())
                        .add("Content", getContent())
                        .toString();
            }

            @Override
            public boolean equals(Object other) {
                if (!(other instanceof Email)) {
                    return false;
                }
                Email otherEmail = (Email) other;

                return Objects.equal(content, otherEmail.getContent())
                        && Objects.equal(subject, otherEmail.getSubject());
            }

            @Override
            public int hashCode() {
                return Objects.hashCode(
                        getSubject(),
                        getContent());
            }
        };
    }
}

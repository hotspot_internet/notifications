package hotspotinternet.notifications.email.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.time.Instant;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.google.common.base.Optional;


public class UsageReportTest {

    @Test
    public void test10PercentRemains() throws Exception {
        String messageText = IOUtils.toString(
                this.getClass().getResourceAsStream("message-10-percent-remains.txt"),
                "UTF-8");
        Optional<UsageReport> usageReport =
                UsageReport.from("ID1", "some source", Instant.EPOCH, messageText);
        assertEquals(
                new UsageReport(null, "ID1", "some source", Instant.EPOCH, null, "0529", 90.0),
                usageReport.get());
    }

    @Test
    public void testNoDataRemains() throws Exception {
        String messageText = IOUtils.toString(
                this.getClass().getResourceAsStream("message-no-data-remains.txt"),
                "UTF-8");
        Optional<UsageReport> usageReport =
                UsageReport.from("ID1", "source", Instant.EPOCH, messageText);
        assertEquals(
                new UsageReport(null, "ID1", "source", Instant.EPOCH, null, "2999", 100.0),
                usageReport.get());
    }

    @Test
    public void testFullNumber() throws Exception {
        Optional<UsageReport> usageReport =
                UsageReport.from(
                        "ID1",
                        "source",
                        Instant.EPOCH,
                        "number (812) 528-3630 has 0% remaining");
        assertEquals(
                new UsageReport(
                        null, "ID1", "source",
                        Instant.EPOCH, null, "(812) 528-3630", 100.0),
                usageReport.get());
    }

    @Test
    public void testFullNumberSpace() throws Exception {
        Optional<UsageReport> usageReport =
                UsageReport.from(
                        "ID1",
                        "source",
                        Instant.EPOCH,
                        "number 528 3630 has 0% remaining");
        assertEquals(
                new UsageReport(null, "ID1", "source", Instant.EPOCH, null, "528 3630", 100.0),
                usageReport.get());
    }

    @Test
    public void testFullNumberDash() throws Exception {
        Optional<UsageReport> usageReport =
                UsageReport.from(
                        "ID1",
                        "source",
                        Instant.EPOCH,
                        "number 528-3630 has 0% remaining");
        assertEquals(
                new UsageReport(null, "ID1", "source", Instant.EPOCH, null, "528-3630", 100.0),
                usageReport.get());
    }

    @Test
    public void testTextNotUsageReport() throws Exception {
        Optional<UsageReport> usageReport = UsageReport.from(
                "ID1",
                "some source",
                Instant.EPOCH,
                "message that doesn't match regex");
        assertFalse(usageReport.isPresent());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBothUsedNonNull() throws Exception {
        new UsageReport(null, "ID1", "some source", Instant.EPOCH, 10L, "0434", 90.0);
    }
}

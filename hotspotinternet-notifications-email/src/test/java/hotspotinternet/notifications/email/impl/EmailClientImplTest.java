package hotspotinternet.notifications.email.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.InOrder;
import org.mockito.Mockito;

import com.diffplug.common.base.Throwing.Specific.Consumer;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.typesafe.config.ConfigFactory;

import hotspotinternet.notifications.email.Email;
import hotspotinternet.notifications.email.EmailClient;

public class EmailClientImplTest {
    private Folder mockFolder;
    private Consumer<Message, MessagingException> mockMessageSender;
    private Store mockStore;
    private EmailClient dut;

    @SuppressWarnings("unchecked")
    @Before
    public void setup() throws Exception {
        mockMessageSender = mock(Consumer.class);
        mockStore = mock(Store.class);
        mockFolder = mock(Folder.class);
        when(mockStore.getFolder("INBOX")).thenReturn(mockFolder);
        dut = createDut(ImmutableMap.of());
    }

    @SuppressWarnings("unchecked")
    private EmailClient createDut(
            ImmutableMap<Message, Optional<UsageReport>> messageToReportMap) throws Exception {
        final Function<Message, Optional<UsageReport>> mockReportFunction = mock(Function.class);
        final Set<Message> messageSet = messageToReportMap.keySet();
        when(mockFolder.getMessages())
                .thenReturn(messageSet.toArray(new Message[messageSet.size()]));

        for (Map.Entry<Message, Optional<UsageReport>> entry : messageToReportMap.entrySet()) {
            when(mockReportFunction.apply(entry.getKey())).thenReturn(entry.getValue());
        }

        return new EmailClientImpl(
                ConfigFactory.load(),
                (properties) -> mockStore,
                mockMessageSender,
                mockReportFunction);
    }

    @Test(expected = IOException.class)
    public void testRunException() throws Exception {
        // given
        dut = new EmailClientImpl(
                ConfigFactory.load(),
                (properties) -> {
                    throw new NoSuchProviderException();
                },
                mockMessageSender,
                message -> Optional.absent());

        // when
        dut.run();
    }

    @Test
    public void testRunAndClose() throws Exception {
        // when
        dut.close();
        dut.run();
        dut.close();

        // then
        final InOrder inOrder = Mockito.inOrder(mockStore, mockFolder);
        inOrder.verify(mockStore).connect(any(String.class), eq("some password"));
        inOrder.verify(mockStore).getFolder("INBOX");
        inOrder.verify(mockFolder).open(Folder.READ_ONLY);
        inOrder.verify(mockFolder).close(false);
        inOrder.verify(mockStore).close();
    }

    @Test
    // smoke test that exception is handled gracefully
    public void testDownloadHandleException() throws Exception {
        // given
        when(mockFolder.getMessages()).thenThrow(MessagingException.class);

        // when
        dut.run();
        assertTrue(dut.getUsageReports().isEmpty());
    }

    @Test
    public void testGetUsageReports() throws Exception {
        // given
        when(mockFolder.isOpen()).thenReturn(true);
        UsageReport usageReport =
                new UsageReport(null, "ID1", "other source", Instant.now(), null, "4345", 10.0);
        UsageReport oldUsageReport =
                new UsageReport(null, "ID2", "source", Instant.EPOCH, null, "0529", 90.0);
        dut = createDut(ImmutableMap.of(
                mock(Message.class), Optional.of(oldUsageReport),
                mock(Message.class), Optional.of(usageReport)));

        // when
        dut.run();

        // then
        assertEquals(
                Arrays.asList(usageReport),
                dut.getUsageReports());
        // opened once during 'run'
        verify(mockFolder).open(Folder.READ_ONLY);
    }

    @Test
    public void testGetUsageReportsFolderNotOpen() throws Exception {
        // given
        when(mockFolder.isOpen()).thenReturn(false);
        UsageReport usageReport =
                new UsageReport(null, "ID1", "other source", Instant.now(), null, "4345", 10.0);
        UsageReport oldUsageReport =
                new UsageReport(null, "ID2", "source", Instant.EPOCH, null, "0529", 90.0);
        dut = createDut(ImmutableMap.of(
                mock(Message.class), Optional.of(oldUsageReport),
                mock(Message.class), Optional.of(usageReport)));

        // when, then
        dut.run();
        assertEquals(
                Arrays.asList(usageReport),
                dut.getUsageReports());
        // opened once during 'run' and again when getting messages
        verify(mockFolder, times(2)).open(Folder.READ_ONLY);
    }

    @Test
    public void testNoUsageReports() throws Exception {
        // given
        dut = createDut(ImmutableMap.of(
                mock(Message.class), Optional.absent(),
                mock(Message.class), Optional.absent()));

        // when, then
        dut.run();
        assertTrue(dut.getUsageReports().isEmpty());
    }

    private static ArgumentMatcher<Message> matches(Email email) {
        return new ArgumentMatcher<Message>() {
            @Override
            public boolean matches(Message message) {
                try {
                    MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
                    return mimeMultipart.getBodyPart(0).getContent().equals(email.getContent())
                            && message.getAllRecipients()[0].equals(
                                    new InternetAddress("some_address@company.com"))
                            && message.getSubject().equals(email.getSubject());
                } catch (Exception e) {
                    fail(e.toString());
                    return false;
                }
            }
        };
    }

    @Test
    public void testSendEmail() throws Exception {
        // given
        dut = createDut(ImmutableMap.of(
                mock(Message.class), Optional.absent(),
                mock(Message.class), Optional.absent()));
        Email email = mock(Email.class);
        when(email.getContent()).thenReturn("content");
        when(email.getSubject()).thenReturn("subject");

        // when
        dut.run();
        dut.send(email);

        // then
        verify(mockMessageSender).accept(argThat(matches(email)));
    }
}

# notifications

Sends and collects notifications.

## Configuration

The following is an example of the properties that should be present in the
application.conf file:

```
notifications.email.address = "some.address@company.com"
notifications.email.password = "some password"

```

Where the address/password are for the email account to which notification
reports are delivered (for example, from cellular service provided).

package hotspotinternet.notifications.main.datatype;

import static com.google.common.base.Preconditions.checkNotNull;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Class for storing information of a data overage.
 */
public final class Overage {

    /**
     * Type of data overage.
     */
    public enum OverageType {
        /** An overage based on data usage within an hour. */
        HOUR(ChronoUnit.HOURS),
        /** An overage based on data usage relative to total cycle limit. */
        CYCLE(ChronoUnit.DAYS);

        private final ChronoUnit chronoUnit;

        private OverageType(final ChronoUnit chronoUnit) {
            this.chronoUnit = chronoUnit;
        }

        /**
         * Gets the minimum interval between consecutive notifications.
         *
         * @return minimum interval between consecutive notifications
         */
        public ChronoUnit getNotificationInterval() {
            return chronoUnit;
        }
    }

    private Long overageId;
    private OverageType overageType;
    private Instant instant;
    private String description;

    // Empty, no-arg constructor for JSON mapping
    public Overage() { }

    /**
     * Constructs Overage from provided parameters.
     *
     * @param description of overage
     * @param overageType type of overage
     * @param instant at which overage calculate was performed
     */
    public Overage(
            final Long overageId,
            final String description,
            final OverageType overageType,
            final Instant instant) {
        this.overageId = checkNotNull(overageId);
        this.description = checkNotNull(description);
        this.overageType = checkNotNull(overageType);
        this.instant = checkNotNull(instant);
    }

    public String getDescription() {
        return description;
    }

    public Long getOverageId() {
        return overageId;
    }

    public OverageType getOverageType() {
        return overageType;
    }

    public Instant getInstant() {
        return instant;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("Overage ID", overageId)
                .add("Description", description)
                .add("Instant", instant)
                .add("Overage Type", overageType)
                .toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Overage)) {
            return false;
        }
        Overage otherOverage = (Overage) other;

        return Objects.equal(overageId, otherOverage.overageId)
                && Objects.equal(description, otherOverage.description)
                && Objects.equal(instant, otherOverage.instant)
                && Objects.equal(overageType, otherOverage.overageType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
                overageId,
                description,
                instant,
                overageType);
    }
}

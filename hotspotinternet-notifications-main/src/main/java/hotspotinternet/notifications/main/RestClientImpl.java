package hotspotinternet.notifications.main;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import com.google.inject.Inject;
import com.typesafe.config.Config;

import hotspotinternet.notifications.email.Email;
import hotspotinternet.notifications.email.EmailClient;
import hotspotinternet.notifications.email.impl.UsageReport;
import hotspotinternet.notifications.main.datatype.Overage;

/**
 * REST client for handling interactions with REST API server.
 */
public class RestClientImpl implements RestClient {
    private static final Logger LOG = LoggerFactory.getLogger(RestClientImpl.class);
    private static final Duration POLL_INTERVAL = Duration.ofMinutes(5);
    private static final String REST_SERVER_KEY = "notifications.rest.server.host";

    private final String restServerHost;
    private final ScheduledExecutorService executorService;
    private final RestTemplate restTemplate;
    private final EmailClient emailClient;

    private Future<?> scheduledFuture;

    @Inject
    RestClientImpl(
            final Config config,
            final EmailClient usageEmailCollector,
            final ScheduledExecutorService executorService,
            final RestTemplate restTemplate) {
        this.restServerHost = config.getString(REST_SERVER_KEY);
        this.emailClient = usageEmailCollector;
        this.executorService = executorService;
        this.restTemplate = restTemplate;
    }

    @Override
    public void run() throws IOException {
        emailClient.run();
        scheduledFuture = executorService.scheduleWithFixedDelay(
                this::poll,
                0,
                POLL_INTERVAL.toMillis(),
                TimeUnit.MILLISECONDS);
    }

    private void poll() {
        postUsageReports();
        getOverages().stream().forEach(this::sendEmail);
    }

    private List<Overage> getOverages() {
        final String url = String.format(
                "%s/notifications/overage",
                restServerHost);
        Overage[] overages = null;
        try {
            overages = restTemplate.getForObject(url, Overage[].class);
        } catch (Throwable ex) {
            LOG.warn("Error getting overages: {}", ex.getMessage());
        }
        return overages == null
                ? Collections.emptyList()
                : Arrays.asList(overages);
    }

    private void sendEmail(final Overage overage) {
        Email email = Email.createEmail(
                overage.getDescription(),
                String.format("%s-based Internet Usage Alert", overage.getOverageType()));
        try {
            emailClient.send(email);
        } catch (MessagingException ex) {
            LOG.warn("Error sending email: {}", ex.getMessage());
        }
    }

    private void postUsageReports() {
        try {
            for (UsageReport usageReport : emailClient.getUsageReports()) {
                handleUsageReport(usageReport);
            }
        } catch (Throwable ex) {
            LOG.warn("Uncaught exception while posting usage reports", ex);
        }
    }

    private UsageReport handleUsageReport(final UsageReport usageReport) {
        final String url = String.format(
                "%s/notifications/usagereport",
                restServerHost);
        LOG.debug("Posting usage report [{}] to: {}", usageReport, url);
        UsageReport responseUsageReport = restTemplate.postForObject(
                url,
                usageReport,
                UsageReport.class);
        LOG.debug("Received response to post: {}", responseUsageReport);
        return responseUsageReport;
    }

    @Override
    public void close() throws IOException {
        if (emailClient != null) {
            emailClient.close();
        }
        if (scheduledFuture != null) {
            scheduledFuture.cancel(true);
        }
    }
}

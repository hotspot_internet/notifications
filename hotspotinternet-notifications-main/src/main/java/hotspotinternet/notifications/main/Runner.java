package hotspotinternet.notifications.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import javax.net.ssl.SSLContext;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Main runner.
 */
@SpringBootApplication
public class Runner {
    @Value("${http.client.ssl.key-store}")
    private String keyStorePath;
    @Value("${http.client.ssl.key-store-password}")
    private String keyStorePassword;
    @Value("${http.client.ssl.trust-store}")
    private String trustStorePath;
    @Value("${http.client.ssl.trust-store-password}")
    private String trustStorePassword;

    /**
     * Main method.
     *
     * @param args for running
     */
    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
    }

    /**
     * Builds rest template.
     *
     * @return built rest template
     */
    @Bean
    RestTemplate restTemplate() throws Exception {
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        try (InputStream inputStream = new FileInputStream(keyStorePath)) {
            keyStore.load(inputStream, keyStorePassword.toCharArray());
        }

        SSLContext sslContext = new SSLContextBuilder()
                .loadTrustMaterial(
                        new File(trustStorePath),
                        trustStorePassword.toCharArray())
                .loadKeyMaterial(
                        keyStore,
                        keyStorePassword.toCharArray())
                .build();
        SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
                sslContext,
                NoopHostnameVerifier.INSTANCE);
        HttpClient httpClient = HttpClients
                .custom()
                .setSSLSocketFactory(socketFactory)
                .build();
        HttpComponentsClientHttpRequestFactory factory =
                new HttpComponentsClientHttpRequestFactory(httpClient);
        return new RestTemplate(factory);
    }

    /**
     * Runs application.
     *
     * @param restTemplate for creating/parsing HTTP messages
     * @return CommandLineRunner for running application
     */
    @Bean
    public CommandLineRunner run(RestTemplate restTemplate) {
        Injector injector = Guice.createInjector(new RunnerModule(restTemplate));
        RestClient restClient = injector.getInstance(RestClient.class);
        return args -> restClient.run();
    }
}

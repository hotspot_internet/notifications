package hotspotinternet.notifications.main;

import java.io.Closeable;
import java.io.IOException;

import com.diffplug.common.base.Throwing.Specific.Runnable;

/**
 * REST client interface for handling interactions with the REST server.
 */
interface RestClient extends Runnable<IOException>, Closeable { }

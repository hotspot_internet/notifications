package hotspotinternet.notifications.main;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.io.IOException;
import java.time.Instant;
import java.util.Collections;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.typesafe.config.ConfigFactory;

import hotspotinternet.notifications.email.Email;
import hotspotinternet.notifications.email.EmailClient;
import hotspotinternet.notifications.email.impl.UsageReport;
import hotspotinternet.notifications.main.datatype.Overage.OverageType;

@SuppressWarnings({"rawtypes", "unchecked"})
public class RestClientImplTest {
    private final ScheduledExecutorService mockExecutorService =
            mock(ScheduledExecutorService.class);
    private final EmailClient emailCollector = mock(EmailClient.class);

    private RestClientImpl dut;
    private MockRestServiceServer mockServer;
    private ScheduledFuture future;

    @BeforeEach
    public void setup() {
        RestTemplate restTemplate = new RestTemplate();
        mockServer = MockRestServiceServer.bindTo(restTemplate).build();
        future = mock(ScheduledFuture.class);
        when(mockExecutorService.scheduleWithFixedDelay(
                argThat(new ArgumentMatcher<Runnable>() {
                    @Override
                    public boolean matches(Runnable argument) {
                        argument.run();
                        return true;
                    }
                }),
                anyLong(),
                anyLong(),
                any())).thenReturn(future);
        dut = new RestClientImpl(
                ConfigFactory.load(),
                emailCollector,
                mockExecutorService,
                restTemplate);
    }

    @Test
    public void testClose() throws Exception {
        // when
        dut.close();
        dut.run();
        dut.close();

        // then
        final InOrder inOrder = Mockito.inOrder(emailCollector, future, mockExecutorService);
        inOrder.verify(emailCollector).run();
        inOrder.verify(mockExecutorService)
                .scheduleWithFixedDelay(any(), anyLong(), anyLong(), any(TimeUnit.class));
        inOrder.verify(emailCollector).close();
        inOrder.verify(future).cancel(true);
    }

    @Test
    public void testDriverRunException() throws Exception {
        doThrow(IOException.class).when(emailCollector).run();
        Assertions.assertThrows(IOException.class, dut::run);
    }

    @Test
    public void testGetUsageReportsException() throws Exception {
        when(emailCollector.getUsageReports())
                .thenAnswer(invocation -> {
                    throw new Throwable();
                })
                .thenReturn(Collections.EMPTY_LIST);
        dut.run();
        dut.run();
        verify(emailCollector, times(2)).getUsageReports();
    }

    private static Matcher<String> jsonMatcher(final JSONObject expectedJsonObject) {
        return new BaseMatcher<String>() {
            @Override
            public boolean matches(Object actual) {
                if (!(actual instanceof String)) {
                    return false;
                }
                JSONObject actualJsonObject = new JSONObject((String) actual);
                return expectedJsonObject.similar(actualJsonObject);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(expectedJsonObject.toString());
            }
        };
    }

    @Test
    public void testPostUsageReports() throws Exception {
        // given
        final String messageId = "some ID";
        final String source = "source";
        final Instant instant = Instant.now();
        final Double usedPercent = 90.0;
        final String phoneNumber = "3432";
        JSONObject expectedJsonObject = new JSONObject()
                .put("usageReportId", JSONObject.NULL)
                .put("messageId", messageId)
                .put("phoneNumber", phoneNumber)
                .put("source", source)
                .put("instant", instant.getEpochSecond() + instant.getNano() / 1.0e9)
                .put("usedBytes", JSONObject.NULL)
                .put("usedPercent", usedPercent);
        JSONObject responseJsonObject = new JSONObject(expectedJsonObject.toString())
                .put("measurementId", 1);
        when(emailCollector.getUsageReports()).thenReturn(Collections.singletonList(
                new UsageReport(
                        null, messageId, source,
                        instant, null, phoneNumber, usedPercent)));

        // when
        mockServer
                .expect(requestTo("https://192.168.151.171:8081/notifications/usagereport"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().string(jsonMatcher(expectedJsonObject)))
                .andRespond(withSuccess(
                        responseJsonObject.toString(),
                        MediaType.APPLICATION_JSON));
        // GET for overage notifications
        mockServer.expect(method(HttpMethod.GET));
        dut.run();

        // then
        verify(emailCollector).run();
        verify(emailCollector).getUsageReports();
        mockServer.verify();
    }

    @Test
    public void testSendEmail() throws Exception {
        // given
        JSONArray overageJsonArray = new JSONArray()
                .put(new JSONObject()
                        .put("description", "some description")
                        .put("overageId", 1)
                        .put("overageType", OverageType.HOUR)
                        .put("instant", Instant.EPOCH));

        // when
        mockServer
                .expect(requestTo("https://192.168.151.171:8081/notifications/overage"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(
                        overageJsonArray.toString(),
                        MediaType.APPLICATION_JSON));
        dut.run();

        // then
        verify(emailCollector).run();
        verify(emailCollector).send(Email.createEmail(
                "some description",
                "HOUR-based Internet Usage Alert"));
        mockServer.verify();
    }
}
